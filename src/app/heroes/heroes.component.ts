import { Component, OnInit } from '@angular/core';
import { Hero } from './hero.model';
import { HeroService } from './hero.service';

@Component({
  moduleId: module.id,
  selector: 'toh-heroes',
  templateUrl: 'heroes.component.html',
  styleUrls: ['heroes.component.css'],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit {
  heroes : Hero[]
  selectedHero : Hero
  constructor(private heroService : HeroService) { }

  ngOnInit() {

      this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
    

    // this.heroes = this.heroService.getHeroes()
    // this.heroes = [
    //   { "id": 11, "name": "Chewbacca", "twitter": "@im_chewy" },
    //   { "id": 12, "name": "Rey", "twitter": "@rey" },
    //   { "id": 13, "name": "Finn (FN2187)", "twitter": "@finn" },
    //   { "id": 14, "name": "Han Solo", "twitter": "@i_know" },
    //   { "id": 15, "name": "Leia Organa", "twitter": "@organa" }
    // ]
  }

  onSelect(hero: Hero) {
    this.selectedHero = hero
  }
}