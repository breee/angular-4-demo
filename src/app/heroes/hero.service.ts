import { Injectable } from '@angular/core';
import { Hero } from './hero.model';
import { Http, Response } from "@angular/http";
import 'rxjs/Rx';

@Injectable()
export class HeroService {
  heroes : Hero[]

  constructor(private http : Http) { }

  getHeroes() {
    return this.http.get('app/heroes.json')
      .map((response: Response) => response.json());
    
    // return [
    //   { "id": 11, "name": "Chewbacca", "twitter": "@im_chewy" },
    //   { "id": 12, "name": "Rey", "twitter": "@rey" },
    //   { "id": 13, "name": "Finn (FN2187)", "twitter": "@finn" },
    //   { "id": 14, "name": "Han Solo", "twitter": "@i_know" },
    //   { "id": 15, "name": "Leia Organa", "twitter": "@organa" }
    // ]
  }
}